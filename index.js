const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

const port = 8000;

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/index.html"))
})

app.get("/about", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/about.html"))
})

app.get("/sitemap", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/sitemap.html"))
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})